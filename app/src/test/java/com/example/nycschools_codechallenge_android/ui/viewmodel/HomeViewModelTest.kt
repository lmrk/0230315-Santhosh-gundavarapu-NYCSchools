package com.example.nycschools_codechallenge_android.ui.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools_codechallenge_android.common.CoroutineTestRule
import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.usecase.NYCHighSchoolUseCase
import com.example.nycschools_codechallenge_android.view.model.SchoolsUiState
import com.example.nycschools_codechallenge_android.view.viewmodel.HomeViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.unmockkAll
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@ExperimentalCoroutinesApi
@OptIn(ExperimentalCoroutinesApi::class)
class HomeViewModelTest {

    @get:Rule
    var coroutineRule = CoroutineTestRule()

    @get:Rule
    var rule = InstantTaskExecutorRule()

    private lateinit var underTests: HomeViewModel

    private val FAKE_ERROR = "UnknownError"

    @MockK
    lateinit var useCase: NYCHighSchoolUseCase

    @MockK
    lateinit var schoolsUiState: SchoolsUiState

    @MockK
    lateinit var schools: List<School>

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        val response = Result.Success(schools)
        coEvery {
            useCase.invoke()
        } coAnswers {
            response
        }
        underTests = HomeViewModel(useCase)
        every { schoolsUiState.schools } answers { schools }
    }

    @Test
    fun `test NYC schools success`() = runTest {
        underTests.getNYCHighSchools()
        advanceUntilIdle()
        val result = underTests.nycSchoolsUiState.value
        assertEquals(result?.schools, schools)
    }

    @Test
    fun `test NYC schools  failure`() = runTest {
        val errorObject = Exception(FAKE_ERROR)
        val response = Result.Error(errorObject)
        coEvery {
            useCase.invoke()
        } coAnswers {
            response
        }

        underTests.getNYCHighSchools()
        advanceUntilIdle()
        val result = underTests.errorMessage.value
        assertNotNull(result)
    }

    @Test
    fun `test NYC schools apiError`() = runTest {
        val errorObject = Exception(FAKE_ERROR)
        val response = Result.ApiError(errorObject)
        coEvery {
            useCase.invoke()
        } coAnswers {
            response
        }

        underTests.getNYCHighSchools()
        advanceUntilIdle()
        val result = underTests.errorMessage.value
        assertNotNull(result)
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}
