package com.example.nycschools_codechallenge_android.domain.usecase

import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.repository.NYCHighSchoolRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertTrue

class SATScoreUseCaseTest {

    private lateinit var useCase: SATScoreUseCase

    @MockK
    lateinit var repository: NYCHighSchoolRepository

    @MockK
    lateinit var satScore: SATScore

    private val provider = Dispatchers.IO

    private val FAKE_DBN = "XXXXX"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        useCase = SATScoreUseCase(
            defaultDispatcher = provider,
            repository
        )
    }

    @Test
    fun `test on success`() = runBlocking {

        val success = Result.Success(satScore)
        coEvery {
            repository.fetchSATScore(FAKE_DBN)
        } returns success

        val result = useCase.invoke(FAKE_DBN)

        assertTrue { result is Result.Success }
    }

    @Test
    fun `test on Error`() = runBlocking {

        val exception = java.lang.Exception()
        val failure = Result.Error(exception)
        coEvery {
            repository.fetchSATScore(FAKE_DBN)
        } returns failure

        val result = useCase.invoke(FAKE_DBN)

        assertTrue { result is Result.Error }
    }

    @Test
    fun `test on ApiError`() = runBlocking {

        val exception = java.lang.Exception()
        val failure = Result.ApiError(exception)
        coEvery {
            repository.fetchSATScore(FAKE_DBN)
        } returns failure

        val result = useCase.invoke(FAKE_DBN)

        assertTrue { result is Result.ApiError }
    }

    @After
    fun tearDown() {
        unmockkAll()
    }
}
