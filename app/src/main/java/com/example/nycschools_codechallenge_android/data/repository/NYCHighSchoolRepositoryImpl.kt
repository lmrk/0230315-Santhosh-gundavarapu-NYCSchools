package com.example.nycschools_codechallenge_android.data.repository

import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.data.service.NYCSchoolService
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.repository.NYCHighSchoolRepository
import com.example.nycschools_codechallenge_android.domain.toSATScore
import com.example.nycschools_codechallenge_android.domain.toSchool
import javax.inject.Inject

class NYCHighSchoolRepositoryImpl @Inject constructor(
    private val nycSchoolService: NYCSchoolService
) : NYCHighSchoolRepository {

    override suspend fun fetchNYCHighSchools(): Result<List<School>> {
        val result = try {
            val nycHighSchools = nycSchoolService
                .getAllNYCHighSchools()
                .mapNotNull { it.toSchool() }
            if (nycHighSchools.isNotEmpty()) {
                Result.Success(nycHighSchools)
            } else {
                Result.ApiError(Exception("No record available"))
            }
        } catch (e: Exception) {
            Result.Error(Exception("Network request failed"))
        }
        return result
    }

    override suspend fun fetchSATScore(dbn: String): Result<SATScore> {
        val result = try {
            val result = nycSchoolService.getSATResults(dbn)
                .mapNotNull { it.toSATScore() }
            if (result.isNotEmpty()) {
                Result.Success(result[0])
            } else {
                Result.ApiError(Exception("No record available"))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Result.Error(Exception("Network request failed"))
        }
        return result
    }
}