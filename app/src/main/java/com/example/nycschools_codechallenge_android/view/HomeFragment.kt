package com.example.nycschools_codechallenge_android.view

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nycschools_codechallenge_android.R
import com.example.nycschools_codechallenge_android.databinding.FragmentHomeBinding
import com.example.nycschools_codechallenge_android.view.adapters.NYCSchoolsListAdapter
import com.example.nycschools_codechallenge_android.view.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * A simple fragment as the default destination for all NYC High Schools.
 */
@AndroidEntryPoint
class HomeFragment : Fragment(), MenuProvider {

    @Inject
    lateinit var adapter: NYCSchoolsListAdapter
    private var _binding: FragmentHomeBinding? = null
    private val viewModel: HomeViewModel by viewModels()
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        binding.adapter = adapter

        val recyclerView = binding.nycschoolRecyclerview
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                DividerItemDecoration.VERTICAL
            )
        )

        viewModel.nycSchoolsUiState.observe(viewLifecycleOwner) {
            adapter.submitList(it.schools)
        }

        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), R.string.something_went_wrong_try_later, Toast.LENGTH_LONG).show()
        }

        adapter.onSchoolClickListener = {
            val direction =
                HomeFragmentDirections.actionHomeFragmentToSchoolDetailsFragment(it)
            findNavController().navigate(direction)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu_main, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            R.id.action_refresh -> {
                viewModel.getNYCHighSchools()
                true
            }
            else -> false
        }
    }
}