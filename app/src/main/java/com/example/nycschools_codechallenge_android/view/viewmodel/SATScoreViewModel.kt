package com.example.nycschools_codechallenge_android.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools_codechallenge_android.data.repository.NYCHighSchoolRepositoryImpl
import com.example.nycschools_codechallenge_android.data.model.SATResult
import com.example.nycschools_codechallenge_android.domain.model.Result.Success
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.usecase.SATScoreUseCase
import com.example.nycschools_codechallenge_android.view.model.SATScoreUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SATScoreViewModel @Inject constructor(private val satScoreUseCase: SATScoreUseCase): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    private val _satScoreResult = MutableLiveData<SATScoreUiState>()
    val satScoreResult: LiveData<SATScoreUiState> = _satScoreResult

    fun getSATScore(dbn: String) {
        viewModelScope.launch {
            when(val result = satScoreUseCase.invoke(dbn)) {
                is Success -> _satScoreResult.value = SATScoreUiState(result.data)
                else -> _errorMessage.value = "Something went wrong. Try again"
            }
        }
    }
}