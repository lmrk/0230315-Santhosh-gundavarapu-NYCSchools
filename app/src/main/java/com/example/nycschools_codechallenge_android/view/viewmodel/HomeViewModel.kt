package com.example.nycschools_codechallenge_android.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools_codechallenge_android.domain.model.Result
import com.example.nycschools_codechallenge_android.domain.model.School
import com.example.nycschools_codechallenge_android.domain.usecase.NYCHighSchoolUseCase
import com.example.nycschools_codechallenge_android.view.model.SchoolsUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val nycHighSchoolUseCase: NYCHighSchoolUseCase): ViewModel() {

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    private val _nycSchoolsUiState = MutableLiveData<SchoolsUiState>()
    val nycSchoolsUiState: LiveData<SchoolsUiState> = _nycSchoolsUiState

    init {
        getNYCHighSchools()
    }

    fun getNYCHighSchools() {
        viewModelScope.launch {
            when(val result = nycHighSchoolUseCase.invoke()) {
                is Result.Success -> _nycSchoolsUiState.value = SchoolsUiState(result.data)
                else -> _errorMessage.value = "Something went wrong. Try again"
            }
        }
    }
}