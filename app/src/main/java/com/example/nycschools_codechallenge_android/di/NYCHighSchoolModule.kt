package com.example.nycschools_codechallenge_android.di

import android.content.Context
import com.example.nycschools_codechallenge_android.data.repository.NYCHighSchoolRepositoryImpl
import com.example.nycschools_codechallenge_android.data.service.NYCSchoolService
import com.example.nycschools_codechallenge_android.domain.repository.NYCHighSchoolRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NYCHighSchoolModule {

    @Provides
    @Singleton
    fun provideNYCHighSchoolRetrofitService(): NYCSchoolService {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/resource/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(NYCSchoolService::class.java)
    }

    @Provides
    fun provideNYCHighSchoolRepository(
        service: NYCSchoolService
    ): NYCHighSchoolRepository = NYCHighSchoolRepositoryImpl(service)

    @Provides
    fun provideDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

}