package com.example.nycschools_codechallenge_android.domain.model

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class ApiError(val exception: Exception) : Result<Nothing>()
    data class Error(val exception: Exception) : Result<Nothing>()
}