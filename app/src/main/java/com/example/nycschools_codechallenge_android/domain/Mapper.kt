package com.example.nycschools_codechallenge_android.domain

import com.example.nycschools_codechallenge_android.data.model.NYCSchoolResult
import com.example.nycschools_codechallenge_android.data.model.SATResult
import com.example.nycschools_codechallenge_android.domain.model.SATScore
import com.example.nycschools_codechallenge_android.domain.model.School

fun NYCSchoolResult.toSchool(): School? {
    if (dbn == null || schoolName == null) {
        return null
    }
    return School(
        dbn = dbn!!,
        schoolName = schoolName!!,
        city = city
    )
}

fun SATResult.toSATScore(): SATScore? {
    if (dbn == null || schoolName == null) {
        return null
    }
    return SATScore(
        dbn = dbn!!,
        schoolName = schoolName!!,
        numOfSATTestTakers = numOfSATTestTakers ?: "--",
        satCriticalReadingAvgScore = satCriticalReadingAvgScore ?: "--",
        satMathAvgScore = satMathAvgScore ?: "--",
        satWritingAvgScore = satWritingAvgScore ?: "--"
    )
}